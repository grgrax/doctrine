<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;
use models\Common\ReportGroup;

class ReportGroupFixture extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager){
		echo "\nAdding Report Groups...\n";
		$yaml = new Parser();
		$reportGroups = $yaml->parse(file_get_contents(__DIR__.'/reportgroup.yml'));

		foreach ($reportGroups as $group) {
			$repGroup = new ReportGroup;
			$repGroup->setName($group['name']);
			$repGroup->setSlug($group['slug']);
			$manager->persist($repGroup);
		}
		$manager->flush();

		echo "\nReport Groups added succesfully.\n";
	}

	public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}
?>