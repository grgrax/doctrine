<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Parser;

class SettingsFixture extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager){
		echo "\nInitializing Options table...\n";
		$yaml = new Parser();
		$settings = $yaml->parse(file_get_contents(__DIR__.'/settings.yml'));

		$id = 1;
		foreach ($settings as $option) {
			$manager->getConnection()->insert('f1_options', array('id'=> $id, 'option_name' => $option['name'], 'option_value'=> $option['value'], 'autoload'=> 1));
			$id++;
		}
		echo "\nOptions table initialized succesfully\n";
	}

	public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}
?>